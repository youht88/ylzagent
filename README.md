#  安装轮子
pip3  install --no-index --find-links=packages/new dist/ylz_agent-0.0.1-py3-none-any.whl

# 注册                                       
export AGENT_BACKEND_SERVICES=192.168.47.238:12800
export AGENT_PROTOCOL=http
export AGENT_CLUSTER=cloud
export AGENT_NAME=cloud_web
export AGENT_NATIVE_OS_NAME=eth0,eth1,ens192,ens33
#暂不使用，以pid替代
export AGENT_INDEX=0
export AGENT_EXPERIMENTAL_FORK_SUPPORT=true
export AGENT_CONFIG_ENDPOINT=http://192.168.47.238:8000
#获取配置请求1分钟
export AGENT_CONFIG_INTERVAL=300
#注册请求5秒
export AGENT_REGISTER_INTERVAL=5
export AGENT_METER_REPORTER_ACTIVE = false
# 执行
python_agent run python test.py


