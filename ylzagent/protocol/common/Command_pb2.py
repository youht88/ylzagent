# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common/Command.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from ..common import Common_pb2 as common_dot_Common__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='common/Command.proto',
  package='skywalking.v3',
  syntax='proto3',
  serialized_options=b'\n+org.apache.skywalking.apm.network.common.v3P\001Z2skywalking.apache.org/repo/goapi/collect/common/v3\252\002\035SkyWalking.NetworkProtocol.V3',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x14\x63ommon/Command.proto\x12\rskywalking.v3\x1a\x13\x63ommon/Common.proto\"K\n\x07\x43ommand\x12\x0f\n\x07\x63ommand\x18\x01 \x01(\t\x12/\n\x04\x61rgs\x18\x02 \x03(\x0b\x32!.skywalking.v3.KeyStringValuePair\"4\n\x08\x43ommands\x12(\n\x08\x63ommands\x18\x01 \x03(\x0b\x32\x16.skywalking.v3.CommandB\x83\x01\n+org.apache.skywalking.apm.network.common.v3P\x01Z2skywalking.apache.org/repo/goapi/collect/common/v3\xaa\x02\x1dSkyWalking.NetworkProtocol.V3b\x06proto3'
  ,
  dependencies=[common_dot_Common__pb2.DESCRIPTOR,])




_COMMAND = _descriptor.Descriptor(
  name='Command',
  full_name='skywalking.v3.Command',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='command', full_name='skywalking.v3.Command.command', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='args', full_name='skywalking.v3.Command.args', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=60,
  serialized_end=135,
)


_COMMANDS = _descriptor.Descriptor(
  name='Commands',
  full_name='skywalking.v3.Commands',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='commands', full_name='skywalking.v3.Commands.commands', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=137,
  serialized_end=189,
)

_COMMAND.fields_by_name['args'].message_type = common_dot_Common__pb2._KEYSTRINGVALUEPAIR
_COMMANDS.fields_by_name['commands'].message_type = _COMMAND
DESCRIPTOR.message_types_by_name['Command'] = _COMMAND
DESCRIPTOR.message_types_by_name['Commands'] = _COMMANDS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Command = _reflection.GeneratedProtocolMessageType('Command', (_message.Message,), {
  'DESCRIPTOR' : _COMMAND,
  '__module__' : 'common.Command_pb2'
  # @@protoc_insertion_point(class_scope:skywalking.v3.Command)
  })
_sym_db.RegisterMessage(Command)

Commands = _reflection.GeneratedProtocolMessageType('Commands', (_message.Message,), {
  'DESCRIPTOR' : _COMMANDS,
  '__module__' : 'common.Command_pb2'
  # @@protoc_insertion_point(class_scope:skywalking.v3.Commands)
  })
_sym_db.RegisterMessage(Commands)


DESCRIPTOR._options = None
# @@protoc_insertion_point(module_scope)
