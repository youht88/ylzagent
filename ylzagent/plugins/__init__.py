#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import inspect
import logging
import pkgutil
import re
import traceback

import pkg_resources
from packaging import version

import ylzagent
from ylzagent import config
from ylzagent.loggings import logger
from ylzagent.utils.comparator import operators
from ylzagent.utils.exception import VersionRuleException

from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime,date

import requests
import base64
import json
import gzip
import psutil
import os
import platform
import time

def get_config():
    try:
        map = {
            "serviceName": config.agent_service_name,  # 是agent_name  #cloud_auth
            "cluster": config.agent_cluster         #cloud
        }
        payload = base64.b64encode(json.dumps(map).encode()).decode()
        url = f"{config.agent_config_endpoint}/admin/agent/getConfig/" + payload

        response = requests.post(url)
        content = response.content.decode("utf-8")
        content_data = json.loads(content)['data']
        #print("config_data=",content_data)
        config.agent_redis_collect = content_data.get("agent.redis.collect","false")
        config.ignore_http_path_with_method = content_data.get("agent.tracing.iginore_endpoint","") #"GET:/system/dept/list,POST:[/system/menu/list]"
        config.ignore_http_host_with_port= content_data.get("agent.ip.limit","")          #"192.168.0.1-192.168.0.225,192.168.1.1-192.168.1.225"
        config.agent_login_request_parms = json.loads(content_data.get("login.request.params", '{}'))  # 'login.request.params'
        config.agent_commit_efficiency = json.loads(content_data.get("commit_efficiency",'{}'))
        config.agent_exclude_db_statements = content_data.get("agent.db.statement","").split(";")
    except Exception as e:
        print("获取配置出错:",e)
        raise

#####register
def get_start_date():
    return datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d")
def get_host_name():
    return os.uname()[1]
def get_system_name():
    return platform.system()
def get_system_arch():
    return platform.machine()
def get_system_version():
    return platform.platform()
def get_system_available_processor():
    return psutil.cpu_count(logical=False)
def get_disk_info():
    disk_info=[]
    # 获取所有磁盘分区的使用情况
    partitions = psutil.disk_partitions()
    for partition in partitions:
        # 获取磁盘分区的使用情况
        usage = psutil.disk_usage(partition.mountpoint)
        disk_info.append(
            {"name":partition.device,"size":usage.total,"use":usage.used}
        )
    return disk_info            
def toKB(value):
    return round(value/1024,2)
def toMB(value):
    return round(value/1024/1024,2)
def get_host_monitor():
    #PHostMonitor()
    def get_net_io_info():
        net_io1 = psutil.net_io_counters()
        time.sleep(1)
        net_io2 = psutil.net_io_counters()
        return {"NET_IN":toKB(net_io2.bytes_recv - net_io1.bytes_recv),"NET_OUT":toKB(net_io2.bytes_sent - net_io1.bytes_sent)}
    def get_disk_io_info():
        disk_io1 = psutil.disk_io_counters()
        time.sleep(1)
        disk_io2 = psutil.disk_io_counters()
        return {"IO_READ":toKB(disk_io2.read_bytes - disk_io1.read_bytes),"IO_WRITE":toKB(disk_io2.write_bytes - disk_io1.write_bytes)}
    def get_cpu_info():
        return {"CPU_PERCENT":psutil.cpu_percent(),"CPU_COUNT":psutil.cpu_count()}
    def get_mem_info():
        mem = psutil.virtual_memory()
        return {"MEM_TOTAL":toMB(mem.total),"MEM_FREE":toMB(mem.free),"MEM_PERCENT":mem.percent}
    net_io_info = get_net_io_info()
    disk_io_info = get_disk_io_info()
    cpu_info = get_cpu_info()
    mem_info = get_mem_info()
    #print("hostmonitor:",net_io_info,disk_io_info,cpu_info,mem_info)
    #封装到PHostMonitor
    host_monitor = []
    host_monitor.append({"type":"NET_IN","value":net_io_info["NET_IN"]})
    host_monitor.append({"type":"NET_OUT","value":net_io_info["NET_OUT"]})
    host_monitor.append({"type":"IO_READ","value":disk_io_info["IO_READ"]})
    host_monitor.append({"type":"IO_WRITE","value":disk_io_info["IO_WRITE"]})
    #host_monitor.append({"type":"CPU_PERCENT","value":cpu_info["CPU_PERCENT"]})
    #host_monitor.append({"type":"CPU_COUNT","value":cpu_info["CPU_COUNT"]})
    #host_monitor.append({"type":"MEM_PERCENT","value":mem_info["MEM_PERCENT"]})
    #host_monitor.append({"type":"MEM_TOTAL","value":mem_info["MEM_TOTAL"]})
    #host_monitor.append({"type":"MEM_FREE","value":mem_info["MEM_FREE"]})
    host_monitor.append({"type":"CPU","value":cpu_info["CPU_PERCENT"]})
    host_monitor.append({"type":"MEM","value":mem_info["MEM_FREE"]})
    return host_monitor
#

def send_base_system():
    try:
        host_name = get_host_name()
        system_name = get_system_name()
        system_version = get_system_version()
        system_arch = get_system_arch()
        system_available_processor = get_system_available_processor()
        disk_info = get_disk_info()
        host_monitor = get_host_monitor()
        start_date = get_start_date()
        #print("base_system:",system_name,system_arch,system_available_processor,disk_info,host_monitor)
        #base_system = PBaseSystem(systemName=system_name,systemArch=system_arch,systemAvailableProcessor=system_available_processor,diskInfo=disk_info,hostMonitor=host_monitor)
        base_system = {
           "service":config.agent_name,
           "serviceInstance":config.agent_instance_name,
           "timestamp":datetime.timestamp(datetime.now()),
           "hostName":host_name,
           "systemName":system_name,
           "systemVersion":system_version,
           "systemArch":system_arch,
           "systemAvailableProcessor":system_available_processor,
           "startDate":start_date,
           "diskInfoVos":disk_info,
           "hostMonitors":host_monitor
        }
        #print("#####","\n",base_system,"\n","#####")
        payload = base64.b64encode(json.dumps(base_system).encode()).decode()
        url = f"{config.agent_config_endpoint}/admin/agent/registerAgent/"+payload
        response = requests.post(url)
    except Exception as e:
        print("发送系统信息错误:",e)
        raise

#def send_host_monitor():
#    try:
#        host_monitor = get_host_monitor()
#        payload = base64.b64encode(json.dumps(host_monitor).encode()).decode()
#        url = f"{config.agent_config_endpoint}/admin/agent/monitorAgent/"+payload
#        response = requests.post(url)
#    except Exception as e:
#        print("发送系统指标数据错误:",e)
#        raise

scheduler = BackgroundScheduler()
scheduler.add_job(get_config, 'interval', seconds=config.agent_config_interval, next_run_time=datetime.now())
scheduler.add_job(send_base_system,'interval',seconds=config.agent_register_interval)
#scheduler.add_job(send_host_monitor,'interval',seconds=config.agent_monitor_interval)

scheduler.start()

def install():
    disable_patterns = config.agent_disable_plugins
    if isinstance(disable_patterns, str):
        disable_patterns = [re.compile(p.strip()) for p in disable_patterns.split(',') if p.strip()]
    else:
        disable_patterns = [re.compile(p.strip()) for p in disable_patterns if p.strip()]
    for importer, modname, _ispkg in pkgutil.iter_modules(ylzagent.plugins.__path__):
        if any(pattern.match(modname) for pattern in disable_patterns):
            logger.info("plugin %s is disabled and thus won't be installed", modname)
            continue
        logger.debug('installing plugin %s', modname)
        plugin = importer.find_module(modname).load_module(modname)

        # todo: refactor the version checker, currently it doesn't really work as intended
        supported = pkg_version_check(plugin)
        if not supported:
            logger.debug("check version for plugin %s's corresponding package failed, thus "
                         "won't be installed", modname)
            continue

        if not hasattr(plugin, 'install') or inspect.ismethod(plugin.install):
            logger.warning("no `install` method in plugin %s, thus the plugin won't be installed", modname)
            continue

        # noinspection PyBroadException
        try:
            plugin.install()
            logger.debug('Successfully installed plugin %s', modname)
        except Exception:
            logger.warning(
                'Plugin %s failed to install, please ignore this warning '
                'if the package is not used in your application.',
                modname
            )
            traceback.print_exc() if logger.isEnabledFor(logging.DEBUG) else None


def pkg_version_check(plugin):
    supported = True

    # no version rules was set, no checks
    if not hasattr(plugin, 'version_rule'):
        return supported

    pkg_name = plugin.version_rule.get('name')
    rules = plugin.version_rule.get('rules')

    try:
        current_pkg_version = pkg_resources.get_distribution(pkg_name).version
    except pkg_resources.DistributionNotFound:
        # when failed to get the version, we consider it as supported.
        return supported

    current_version = version.parse(current_pkg_version)
    # pass one rule in rules (OR)
    for rule in rules:
        if rule.find(' ') == -1:
            if check(rule, current_version):
                return supported
        else:
            # have to pass all rule_uint in this rule (AND)
            rule_units = rule.split(' ')
            results = [check(unit, current_version) for unit in rule_units]
            if False in results:
                # check failed, try to check next rule
                continue
            else:
                return supported

    supported = False
    return supported


def check(rule_unit, current_version):
    idx = 2 if rule_unit[1] == '=' else 1
    symbol = rule_unit[0:idx]
    expect_pkg_version = rule_unit[idx:]

    expect_version = version.parse(expect_pkg_version)
    f = operators.get(symbol) or None
    if not f:
        raise VersionRuleException(f'version rule {rule_unit} error. only allow >,>=,==,<=,<,!= symbols')

    return f(current_version, expect_version)


def check_ip_in_range(ip, ip_ranges):  #ignore 用
    ip_parts = ip.split('.')
    ip_int = int(ip_parts[0]) * 256**3 + int(ip_parts[1]) * 256**2 + int(ip_parts[2]) * 256 + int(ip_parts[3])
    ip_ranges = ip_ranges.split(',')
    for ip_range in ip_ranges:
        if '-' in ip_range:
            start, end = ip_range.split('-')
            start_parts = start.split('.')
            end_parts = end.split('.')
            start_int = int(start_parts[0]) * 256**3 + int(start_parts[1]) * 256**2 + int(start_parts[2]) * 256 + int(start_parts[3])
            end_int = int(end_parts[0]) * 256**3 + int(end_parts[1]) * 256**2 + int(end_parts[2]) * 256 + int(end_parts[3])
            if start_int <= ip_int <= end_int:
                return True
        else:
            if ip == ip_range:
                return True
    return False

def ignore_http_request(host:str,path:str,method:str):
    # print(2828289,host,path)
    _host = host
    _path = path.split('?')[0]
    if not _path.startswith('/'):
        _path = '/'+_path
    _method = method.upper()

    _ignore_http_path_with_method = config.ignore_http_path_with_method   # "GET:/system/dept/list,GET:/system/menu/list"
    #print("1",_ignore_http_path_with_method)
    if _ignore_http_path_with_method!='':
        conditions_path = list(map(lambda item: item.split(':'),_ignore_http_path_with_method.split(',')))
        #print("2",conditions_path)
        for condition in conditions_path :
            #print("3",condition,_method,_path)
            if not re.match(r"^\[.*\]$",condition[1]):
                #print("not re.match",condition[1],_path)
                if condition[0].upper() == _method and _path == condition[1]:
                    return True
            else:
                #print("re.match",condition[1][1:-1],_path)
                if condition[0].upper() == _method and re.match(condition[1][1:-1],_path):
                    return True
    # localhost:8888
    _ignore_http_host_with_port = config.ignore_http_host_with_port
    if _ignore_http_host_with_port!='' and check_ip_in_range(_host,_ignore_http_host_with_port):
        return True

    return False

def tag_loggin_session(host:str,path:str,method:str,headers,cookies,req_args,req_data):   #request
    session=""
    username=""

    loginPath = config.agent_login_request_parms.get('loginPath','')
    sessionKey = config.agent_login_request_parms.get('sessionKey','')
    tokenPrefix = config.agent_login_request_parms.get('tokenPrefix','')
    loginParmasAuthKey = config.agent_login_request_parms.get('loginParmasAuthKey','')
    cookieSessionKey = config.agent_login_request_parms.get('cookieSessionKey','')
    cookieSessionUserKey = config.agent_login_request_parms.get('cookieSessionUserKey','')
    #print("1 tag_login_session","host=",host,"path=",path,"method=",method,"headers=",headers,"cookies=",cookies,
    #        "addons=[",cookies.get("session","ERR"),cookies.get("_dataflulx_usr_id","ERR"),"]")
    #print("2 tag_login_session","loginPath=",loginPath,"sessionKey=",sessionKey,"tokenPrefix=",tokenPrefix,
    #          "loginParmasAuthKey=",loginParmasAuthKey,"cookieSessionKey=",cookieSessionKey,
    #          "cookieSessionUserKey=",cookieSessionUserKey)
    
    # loginpath符合
    try:
        loginPath = loginPath.split(':')[0].upper() + ':' + loginPath.split(':')[1]
        if loginPath == method.upper() +":"+path:
            #print("login","req.args",req_args)
            #print("login","req.data",req_data)
            username = req_data.get(loginParmasAuthKey,'')
            print("login username===>",username)
    except Exception as e:
        #print("错误！！",e)
        username=""    
    # headers
    headers_session = headers.get(sessionKey,'')
    #print("2 check headers",headers_session,sessionKey)
    if headers_session!='' and headers_session.startwith(tokenPrefix):   #去掉ba
        session = headers_session.replace(tokenPrefix,'')
        if not username:
            username = headers.get(loginParmasAuthKey,'')
        return {"session": session, "loginUser": username}
    # cookies
    cookies_session = cookies.get(cookieSessionKey, '')
    #print("3 cookies_session",cookies_session,cookieSessionKey,cookies.get(cookieSessionUserKey, ''),cookieSessionUserKey)
    if cookies_session !='':
        session = cookies_session
        if not username:
            username = cookies.get(cookieSessionUserKey, '')
        return {"session": session, "loginUser": username}
    return None

def tag_return_value(response_data,response_content_type,path=None):     #response
    responseTokenType = config.agent_login_request_parms.get('responseTokenType','JSON')
    responseTokenKey = config.agent_login_request_parms.get('responseTokenKey','')

    printReturnValue = config.agent_login_request_parms.get('printReturnValue','false')
    #print("0",type(config.agent_login_request_parms))    
    #printReturnValue='true'
    #responseTokenType="JSON"
    #print("="*80,printReturnValue,responseTokenType,response_content_type)
    try:
        returnValueAllCollect = int(config.agent_login_request_parms.get('returnValueAllCollect','1'))
    except:
        returnValueAllCollect = 0
    # encryptDocument = config.agent_login_request_parms['lencryptDocument']
    #print('responseTokenType',responseTokenType)
    #print('responseTokenKey',responseTokenKey)
    #print('printReturnValue',printReturnValue)
    #print('returnValueAllCollect',returnValueAllCollect)
    #print("1","printReturnValue",printReturnValue)
    if printReturnValue.lower() == 'false':
        return None
    if responseTokenType.upper() != 'JSON' or 'application/json' not in response_content_type.lower():
        return None
    try:
        if type(response_data)==bytes:
            data = response_data.decode()
        elif type(response_data)==list or type(response_data)==dict:
            data = json.dumps(response_data,cls=DateTimeEncoder)
        elif type(response_data)==str:
            data = response_data
        else:
            data = None
    except Exception as e1:
        #print("数据处理？？",e1,type(response_data))
        try:
            data = str(gzip.decompress(response_data).decode())
        except Exception as e2:
            #print("未知数据类型？？",e2,type(response_data),response_data)
            data=None
    if data==None:
        return None
    try:
        #print("3","result=>",str(data)[:20])
        #data = json.loads(re.sub(r'\\u([0-9a-fA-F]{4})',lambda x: chr(int(x.group(1),16)), str(data)))
        #returnValueAllCollect=2
        data_json  = json.loads(data)
        #print("return value1,data_json->",type(data_json),data_json)
        data_filter = json_filter(data_json,returnValueAllCollect,0)
        #print("return value2,data_filter->",type(data_filter),data_filter)
        data_trans = re.sub(r'\\u([0-9a-fA-F]{4})',lambda x: chr(int(x.group(1),16)), json.dumps(data_filter))
        #print("return value3,data_trans==>",type(data_trans),data_trans)
        return data_trans
    except Exception as e:
        print("error on result==>",e)
        return None

class DateTimeEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj,date):
            return obj.strftime('%Y-%m-%d')
        return json.JSONEncoder.default(self,obj)

def json_filter(data,list_limit:int = 0, string_limit:int = 0):
    if type(data)==list:
        _list_limit = len(data) if list_limit==0 else list_limit
        data = data[:_list_limit]
        for idx,item in enumerate(data):
            data[idx] = json_filter(item,list_limit,string_limit)
        #if len(data) > _list_limit:
        #    data.append("...")
        return data
    elif type(data)==str:
        _string_limit = len(data) if string_limit==0 else string_limit
        return data[:_string_limit] + ("..." if len(data)>_string_limit else '')
    elif type(data)==dict:
        for key in data:
            data[key] = json_filter(data[key],list_limit,string_limit)
        return data
    else:
        return data
