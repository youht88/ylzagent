from typing import Optional

from ylzagent import Component, Layer, config
from ylzagent.trace import tags
from ylzagent.trace.carrier import Carrier
from ylzagent.trace.context import get_context,NoopContext
from ylzagent.trace.span import NoopSpan
from ylzagent.trace.tags import TagHttpMethod, TagHttpURL, TagHttpStatusCode, TagHttpParams,Tag
from ylzagent.plugins import ignore_http_request,tag_loggin_session,tag_return_value
import datetime
import pdb
import sys
import json
def install():
    import quart
    from quart import Quart
    
    _full_dispatch_request = Quart.full_dispatch_request
    _handle_user_exception = Quart.handle_user_exception
    _handle_exception = Quart.handle_exception

    def params_tostring(params):
        return "\n".join([k + '=[' + ",".join(params.getlist(k)) + ']' for k, _ in params.items()])

    def __get_client_ip(request):
        '''
        获取客户端IP
        '''
        keys = ["x-forwarded-for", 'x-real-ip']
        for key, value in request.headers.items():
            if key.lower().strip() in keys:
                return value
        return request.remote_addr

    async def _sw_full_dispatch_request(this: Quart, request_context: Optional[quart.app.RequestContext] = None):
        import quart
        #print('in_quart','*'*100)
        req = quart.request
        method = req.method

        headers = req.headers
        cookies = req.cookies
        #if 'static/portal' not in req.path:
        #    print(datetime.datetime.now(),"#"*100,"_sw_full_dispatch_request",req.headers,req.path,str(get_context()),"标头")
        
        #import traceback
        #traceback.print_stack()
        #print("#"*100,'taceback_end')
        remote_add = __get_client_ip(req)
        remote_port = ""
        if "REMOTE_PORT" not in req.headers:
            remote_port = ""

        try:
            req_args = req.args
            req_data = await req.json
            loggin_session = tag_loggin_session(req.host, req.path, method, headers, cookies,req_args,req_data)
            #print("ag_quart==>",req.path,headers.get('X-Datadog-Parent-Id',None),headers.get('X-Datadog-Trace-Id',None),headers.get('Cookie',None))
        except Exception as e:
            print("loggin_session error====>",e)

        ext_user=json.dumps({
               "requestIp":remote_add,
               "user": loggin_session.get('loginUser','') if loggin_session else "",
               "session": loggin_session.get('session','') if loggin_session else ""
             })
        carrier = Carrier(correlation={'ext_user':ext_user})
        for item in carrier:
            if item.key.capitalize() in req.headers:
                item.val = req.headers[item.key.capitalize()]

        ignore = ignore_http_request(req.host, req.path.split('?')[0], method)
        if ignore:
            span = NoopSpan(NoopContext())
        else:
            span = NoopSpan(NoopContext()) if config.ignore_http_method_check(method) \
                else get_context().new_entry_span(op=f"{method.upper()}:{req.path}", carrier=carrier, inherit=Component.General)

        if loggin_session:
            print("in_span_quart",loggin_session,carrier.val)

        with span:
            span.layer = Layer.Http
            span.component = Component.Flask
            span.peer = '%s:%s' % (
                remote_add,
                remote_port
            )
            span.tag(TagHttpMethod(method))
            span.tag(TagHttpURL(req.url.split("?")[0]))
            
            span.tag(Tag(str(remote_add)).custom_key("requestIp"))
            
            if loggin_session:
                span.tag(Tag(str(loggin_session.get('session','ERR'))).custom_key("session"))
                span.tag(Tag(str(loggin_session.get('loginUser','ERR'))).custom_key("loginUser"))
            
            span.tag(Tag(str(headers)).custom_key("headers"))            
            
            values = await req.values
            
            
            if config.plugin_flask_collect_http_params and values:
                span.tag(TagHttpParams(params_tostring(req.values)[0:config.plugin_http_http_params_length_threshold]))
                     
            resp = await _full_dispatch_request(this, request_context=request_context)
            #if 'login' not in req.path:
            #   pdb.set_trace()
            if resp.status_code >= 400:
                span.error_occurred = True

            span.tag(TagHttpStatusCode(resp.status_code))
            
            try:
                resp_content_type = resp.content_type
                if 'application/json' in resp_content_type :
                    resp_data = await resp.get_data()
                    return_value = tag_return_value(resp_data,resp_content_type,path=req.path)
                    if return_value != None:  ####
                        span.tag(Tag(return_value).custom_key("returnValue"))
            except:
                pass
            return resp

    def _sw_handle_user_exception(this: Quart, e):
        if e is not None:
            entry_span = get_context().active_span
            if entry_span is not None and type(entry_span) is not NoopSpan:
                entry_span.raised()

        return _handle_user_exception(this, e)

    async def _sw_handle_exception(this: Quart, e):
        if e is not None:
            entry_span = get_context().active_span
            if entry_span is not None and type(entry_span) is not NoopSpan:
                entry_span.raised()

        return await _handle_exception(this, e)

    Quart.full_dispatch_request = _sw_full_dispatch_request
    Quart.handle_user_exception = _sw_handle_user_exception
    Quart.handle_exception = _sw_handle_exception
