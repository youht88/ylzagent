from ylzagent import Layer, Component
from ylzagent.trace.context import get_context
from ylzagent.trace.tags import TagHttpMethod, TagHttpURL, TagHttpStatusMsg
import sys

def sw_trace_function(func):
        def _wrap(*args, **kwargs):
            type = func.__class__.__name__
            print("被wrap的函数",f'{type},{func.__module__},{func.__name__},{sys._getframe().f_code.co_filename}')
            try:
                print("上一级信息-->",f'{sys._getframe(1).f_code.co_filename},{sys._getframe(1).f_code.co_name},{sys._getframe(1).f_locals.items()}')
                print("上两级信息-->",f'{sys._getframe(2).f_code.co_filename},{sys._getframe(2).f_code.co_name},{sys._getframe(2).f_locals.items()}')
            except:
                pass
            if type!='method' and type!='function':
                 return func(*args, **kwargs)
            if type=='method':
                # span 逻辑
                span = get_context().new_local_span(op=f"[method]:{func.__module__}.{func.__self__.__class__.__name__}.{func.__name__}")
            else:
                span = get_context().new_local_span(op=f"[function]:{func.__module__}.{func.__name__}") 
            with span:
                print("sw_trace_function:",str(span))
                span.layer = Layer.Http
                return func(*args, **kwargs)
        return _wrap
    # 把sw_trace_function转换为全局变量,从而可以在工程的任何地方调用@sw_trace_function装饰器，实际使用中不安全。应采用模块导出的方式
    # globals()['sw_trace_function'] = sw_trace_function  
def install():
    print("start install traceFunction plugin")
    print("finish install traceFunction plugin")
    
