from ylzagent import Layer, Component, config
from ylzagent.trace.context import get_context
from ylzagent.trace.tags import Tag,TagDbType, TagDbInstance, TagDbStatement, TagDbSqlParameters

def install():
    from sqlalchemy import engine

    _execute = engine.Connection.execute

    def _sw_execute(self,statement,*multiparams,**params):
        peer = f'{self.engine.url.host}:{self.engine.url.port}'
        drivername = self.engine.url.drivername
        if ("mysql" in drivername) or str(statement) in config.agent_exclude_db_statements:
            return _execute(self, statement, *multiparams,**params)
        context = get_context()
        with context.new_exit_span(op='Mssql/sqlalchemy/execute', peer=peer, component=Component.PyMysql) as span:
            span.layer = Layer.Database
            res = _execute(self, statement, *multiparams,**params)

            span.tag(TagDbType('mssql'))
            span.tag(Tag(drivername).custom_key("driver"))
            span.tag(TagDbInstance(self.engine.url.database))
            span.tag(TagDbStatement(str(statement)))
            if multiparams:
                args = multiparams
            elif params:
                args = params
            else:
                args = []
            if config.plugin_sql_parameters_max_length and args:
                parameter = ','.join([str(arg) for arg in args])
                max_len = config.plugin_sql_parameters_max_length
                parameter = f'{parameter[0:max_len]}...' if len(parameter) > max_len else parameter
                span.tag(TagDbSqlParameters(f'[{parameter}]'))

            return res

    engine.Connection.execute = _sw_execute
