#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from ylzagent import Layer, Component, config
from ylzagent.trace.carrier import Carrier
from ylzagent.trace.context import get_context, NoopContext
from ylzagent.trace.span import NoopSpan
from ylzagent.trace.tags import TagHttpMethod, TagHttpURL, TagHttpStatusCode, TagHttpParams,Tag
from ylzagent.plugins import ignore_http_request,tag_loggin_session,tag_return_value
import time
link_vector = ['https://flask.palletsprojects.com']
support_matrix = {
    'flask': {
        '>=3.7': ['2.0']  # 1.x removed due to EOL
    }
}
note = """"""

def install():
    from flask import Flask
    _full_dispatch_request = Flask.full_dispatch_request
    _handle_user_exception = Flask.handle_user_exception
    _handle_exception = Flask.handle_exception

    def params_tostring(params):
        return '\n'.join([f"{k}=[{','.join(params.getlist(k))}]" for k, _ in params.items()])

    def _sw_full_dispatch_request(this: Flask):
        import flask

        req = flask.request
        carrier = Carrier()
        method = req.method

        headers = req.headers
        cookies = req.cookies
        # print('header',headers)
        # print('cookies', cookies)
        #print("#"*100,"traceback_start")        
        #import traceback
        #traceback.print_stack()
        #print("#"*100,'taceback_end')

        for item in carrier:
            if item.key.capitalize() in req.headers:
                item.val = req.headers[item.key.capitalize()]

        # print("path= ", req.path.split('?')[0])
        # print("method= ", method)
        # print('host= ', req.host)
        # print(' ',)

        # if req.path.split('?')[0] == "/caculate_10g":
        # print(282828,req.host, req.path.split('?')[0], method)
        
        ignore = ignore_http_request(req.host, req.path.split('?')[0], method)
        if ignore:
            span = NoopSpan(NoopContext())

        else:
            span = NoopSpan(NoopContext()) if config.ignore_http_method_check(method) \
                else get_context().new_entry_span(op=req.path, carrier=carrier, inherit=Component.General)
        
        with span:
            span.layer = Layer.Http
            span.component = Component.Flask
            if all(environ_key in req.environ for environ_key in ('REMOTE_ADDR', 'REMOTE_PORT')):
                span.peer = f"{req.environ['REMOTE_ADDR']}:{req.environ['REMOTE_PORT']}"
            span.tag(TagHttpMethod(method))
            span.tag(TagHttpURL(req.url.split('?')[0]))

            #判断 ###
            req_args = req.args
            req_data = None
            loggin_session = tag_loggin_session(req.host,req.path,method,headers,cookies,req_args,req_data)
            if loggin_session:
                span.tag(Tag(loggin_session['session'])).custom_key("session")
                span.tag(Tag(loggin_session['loginUser'])).custom_key("loginUser")

            if config.plugin_flask_collect_http_params and req.values:
                span.tag(TagHttpParams(params_tostring(req.values)[0:config.plugin_http_http_params_length_threshold]))
            # span.tag(Tag("abc").custom_key("abc_key"))

            resp = _full_dispatch_request(this)

            if resp.status_code >= 400:
                span.error_occurred = True

            span.tag(TagHttpStatusCode(resp.status_code))
            return_value=None
            if "application/json" in resp.content_type:
                return_value = tag_return_value(resp.data,resp.content_type,req.path)
            if return_value != None:  ####
                span.tag(Tag(return_value).custom_key("returnValue"))

            return resp

    def _sw_handle_user_exception(this: Flask, e):
        if e is not None:
            entry_span = get_context().active_span
            if entry_span is not None and type(entry_span) is not NoopSpan:
                entry_span.raised()

        return _handle_user_exception(this, e)

    def _sw_handle_exception(this: Flask, e):
        if e is not None:
            entry_span = get_context().active_span
            if entry_span is not None and type(entry_span) is not NoopSpan:
                entry_span.raised()

        return _handle_exception(this, e)

    Flask.full_dispatch_request = _sw_full_dispatch_request
    Flask.handle_user_exception = _sw_handle_user_exception
    Flask.handle_exception = _sw_handle_exception
