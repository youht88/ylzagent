import asyncio
from ylzagent.trace.carrier import Carrier
from ylzagent import Layer, Component
from ylzagent.trace.context import get_context
from ylzagent.trace.tags import TagHttpMethod, TagHttpURL, TagHttpStatusMsg,Tag
import time
import json
from ylzagent.plugins import tag_loggin_session,tag_return_value

def install():
    print("net plugin install","*"*20)
    from dbfortess.reactnet.vlan import VLANEngine
    from dbfortess.reactnet.client import VLANRequest
    from dbfortess.rpc.web import WebService

    __on_switch_received__ = VLANEngine.__on_switch_received__    
    __call__ = VLANRequest.__call__
    
    on_rpc_vlan_request = VLANEngine.on_rpc_vlan_request
    __request__ = WebService.__request__

    agent_key = 'Sw8'

    async def _sw_on_rpc_vlan_request(self, client, requestid, command, peerid, vlan, action, *frames):
        #print("*"*30,command,vlan,action,len(frames))
        op="rpc-server"
        carrier_val=""
        try:
            op ="[rpc_server]:" + vlan.decode()+"@"+ action.decode()
            _frames = unpackb(frames[0])
            _headers = _frames.get("kwargs",{}).get("_request_scope",{}).get("headers",[])
            for item in _headers:
              if item[0].decode() == agent_key:
                  carrier_val = item[1].decode()
        except Exception as e:
            print("error on _sw_on_rpc_vlan_request",e)
        context = get_context()
        carrier = Carrier()
        #print("!!!!! carrier_val=",carrier_val)
        if carrier_val:
            carrier.val = carrier_val
        with context.new_entry_span(op=op, carrier = carrier) as span:
            span.layer = Layer.MQ
            span.component = Component.RabbitmqConsumer
            #span.tag(Tag(str(frames)).custom_key("frames"))
            
            res = await on_rpc_vlan_request(self, client, requestid, command, peerid, vlan, action, *frames)
            try:
                #print("CARRIER VAL",carrier._ext_user,type(carrier._ext_user))
                if carrier._ext_user:
                    ext_user = json.loads(carrier._ext_user)
                    if "requestIp" in ext_user:
                        span.tag(Tag(str(ext_user["requestIp"])).custom_key("requestIp"))
                    if "user" in ext_user:
                        span.tag(Tag(str(ext_user["user"])).custom_key("user"))
                    if "session" in ext_user:
                        span.tag(Tag(str(ext_user["session"])).custom_key("session"))
            except Exception as e:
                print("rpc_server in ag_net plugins ext_user error",e)
            try:
                data = dict(unpackb(res[5]))
                if type(data)==list or type(data)==dict:
                    #print("rpc server1===>",type(res),res)
                    return_value = tag_return_value(data,'application/json',path=None)
                    #return_value = None
                     #print("rpc server2===>",return_value)
                    if return_value:
                        span.tag(Tag(return_value).custom_key("returnValue"))
            except Exception as e:
                print("rpc_server in ag_net plugins unpack error",e) 
            return res

    async def _sw__on_switch_received__(self, client, frames):
        try:
            print("^"*30,"_sw_switch_receive",len(frames),"|",frames[1])
            if len(frames)!=6:
                print("frames op =====",len(frames),frames[1])
                return await __on_switch_received__(self, client, frames)
            if len(frames)==6:
                _arg0,bcommand,_arg1,bvlan,baction,bparams = frames
                print(datetime.datetime.now(),"frames bcommand,bvalan,baction",bcommand,bvlan,baction,str(frames)[:50])
                return await __on_switch_received__(self, client, frames)
            vlan = bvlan.decode()
            action = baction.decode()
            op ="[rpc_server]:" + vlan+"@"+ action
            #print("*"*30,"receive",len(frames),"@",bvlan,baction,op)
            params = unpackb(bparams)
            kwargs = params.get('kwargs',{})
            carrier_val = kwargs.get('headers',{}).get(agent_key,None)
             
            context = get_context()
            carrier = Carrier()
        except Exception as e:
            print("????***",e)
        if carrier_val:
            carrier.val = carrier_val
            print("????",kwargs.keys())
            del kwargs['headers'][agent_key]
            frames = (_arg0,bcommand,_arg1,bvlan,baction,packb(params))
        #if vlan == "rpc/portal" and action == "static":
        #    return await __on_switch_received__(self, client, frames)

        with context.new_entry_span(op=op, carrier = carrier) as span:
            span.layer = Layer.MQ
            span.component = Component.RabbitmqConsumer
            span.tag(Tag(str(frames)).custom_key("frames")) 
            return await __on_switch_received__(self, client, frames)

    async def _sw__call__(self, *args, **kwargs):
        #print("^"*100,"_sw__call__",args,"|",kwargs)
        try:
            _scope = kwargs.get("_request_scope",{})
            _path = _scope.get("path","")
            if _path=="" or  '/static/portal/' in _path:
                return await __call__(self,*args,**kwargs)
            print("!"*50,"_sw__call__",f'{args}|{_scope.get("method")}|{_scope.get("path")}|{_scope.get("server")}')
            context = get_context()
            op = f"call:[rpc-{_scope.get('method')}]:/{_scope.get('path')}"
            peer  = str(_scope.get("server"))
            with context.new_exit_span(op=op,peer = peer) as span:
                span.layer = Layer.MQ
                span.component = Component.RabbitmqProducer
                carrier = span.inject()
                carrier_val =carrier.val
                #kwargs.get('headers',{})[agent_key] = carrier_val

                ###
                _scope.get('headers',[]).append((agent_key.encode('utf8'),carrier_val.encode('utf8')))
                span.tag(Tag(str(kwargs)).custom_key("kw_args"))

                return await __call__(self,*args,**kwargs)
        except Exception as e:
            print("_sw__call__ ???",e)

    async def _sw__request__(self, *args, **kwargs):
        #print("^"*100,"_sw__request__",args,"|",kwargs)
        try:
            _scope = kwargs.get("_request_scope",{})
            _path = _scope.get("path","") 
            if _path=="" or  '/static/portal/' in _path or not _scope:
                return await __request__(self,*args,**kwargs)
            context = get_context()
            op = f"[rpc-{_scope.get('method')}]:/{_scope.get('path')}"
            peer  = str(_scope.get("server"))
            with context.new_exit_span(op=op,peer = peer) as span:
                span.layer = Layer.MQ
                span.component = Component.RabbitmqProducer
                carrier = span.inject()
                carrier_val =carrier.val
                #kwargs.get('headers',{})[agent_key] = carrier_val
            
                ###
                _scope.get('headers',[]).append((agent_key.encode('utf8'),carrier_val.encode('utf8')))
                #span.tag(Tag(str(kwargs)).custom_key("kw_args"))

                return await __request__(self,*args,**kwargs)
        except Exception as e:
            #print("_sw__request__ ???",e)
            raise e
        
    #VLANEngine.__on_switch_received__ =_sw__on_switch_received__ 
    #VLANRequest.__call__ = _sw__call__ 
    WebService.__request__ = _sw__request__
    VLANEngine.on_rpc_vlan_request = _sw_on_rpc_vlan_request
    print("net plugin finish","*"*20)
