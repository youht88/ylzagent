import asyncio
from ylzagent.trace.carrier import Carrier
from ylzagent import Layer, Component
from ylzagent.trace.context import get_context
from ylzagent.trace.tags import TagHttpMethod, TagHttpURL, TagHttpStatusMsg,Tag
import time
import json
import datetime
import inspect
def snap_args(args,num=20):
    #打印参数的缩略信息
    return '|'.join(list(map(lambda item:str(item)[:num],args)))
def snap_kwargs(kwargs,num=20):
    #打印参数的缩略信息
    return '|'.join(list(map(lambda item:str({str(item):str(kwargs[item])[:num]}),kwargs)))
def parse_op(opeval,func_name,span_type,args,kwargs):
    op = ""
    try:
        op = eval(opeval)
    except Exception as e:
        if opeval:
            print("error on op",opeval,e)
        op = f"[{span_type}]:{func_name}"
    return op
def parse_peer(peereval,func_name,span_type,args,kwargs):
    peer = ""
    try:
        peer = eval(peereval)
    except Exception as e:
        if peereval:
            print("error on peer",peereval,e)
        peer = f"peer unknow"
    return peer
def parse_filter(filtereval,func_name,span_type,args,kwargs):
    try:
        if filtereval:
            if eval(filtereval):
                return True
    except Exception as e:
        print("error on filter",filtereval,e)
    return False
def parse_tags(tagseval,func_name,span_type,args,kwargs):
    tags={}
    try:
        if tagseval:
           tags = eval(tagseval)
    except Exception as e:
        print("error on tags:",tagseval,e)
    return tags
def install():
    print("test plugin install","*"*20)
    def wrap(func, note="",num=20,span_type=None,carrier_key="##agent_key##",
             filtereval = None,tagseval=None,
             layer=Layer.Unknown,component=Component.General,
             opeval=None,peereval=None,carriereval=None,imports=None):
        
        def sync_wrapper(*args,**kwargs): 
            if imports:
                for lib in imports:
                    importlib.import_module(lib)
            func_name = func.__name__
            op = parse_op(opeval,func_name,span_type,args,kwargs)
            peer = parse_peer(peereval,func_name,span_type,args,kwargs)
            if parse_filter(filtereval,func_name,span_type,args,kwargs):
                return func(*args, **kwargs)
            try:
                print("查看",datetime.datetime.now(),"note=",note,"op=",op,"peer=",peer,"func_name=",func_name,"args=",snap_args(args,num),"kwargs=",snap_kwargs(kwargs,num))
            except Exception as e:
                print("错误",e,op,peer,args,kwargs)
            tags = parse_tags(tagseval,func_name,span_type,args,kwargs)
            if span_type=="local":
                context = get_context()    
                with context.new_local_span(op=op) as span:
                    span.layer = layer
                    span.component = component
                    for key in tags:
                        span.tag(Tag(str(tags[key])).custom_key(str(key)))
                    return func(*args, **kwargs)
            elif span_type=="exit":
                context = get_context()
                with context.new_exit_span(op=op, peer = peer) as span:
                    span.layer = layer
                    span.component = component
                    carrier = span.inject()
                    #kwargs[carrier_key] = carrier.val
                    return func(*args, **kwargs)
            elif span_type=="entry":
                context = get_context()
                carrier = Carrier()
                if kwargs.get(carrier_key):
                    carrier.val = kwargs[carrier_key]
                with context.new_entry_span(op=op, carrier = carrier) as span:
                    span.layer = layer
                    span.component = component
                    return func(*args, **kwargs)
            else:
                return func(*args, **kwargs)
        async def async_wrapper(*args,**kwargs): 
            if imports:
                for lib in imports:
                    importlib.import_module(lib)
            func_name = func.__name__
 
            op = parse_op(opeval,func_name,span_type,args,kwargs)
            peer = parse_peer(peereval,func_name,span_type,args,kwargs)
            if parse_filter(filtereval,func_name,span_type,args,kwargs):            
                return await func(*args, **kwargs)
            try:
                print("查看",datetime.datetime.now(),"note=",note,"op=",op,"peer=",peer,"func_name=",func_name,"args=",snap_args(args,num),"kwargs=",snap_kwargs(kwargs,num))
            except Exception as e:
                print("错误",e)
            tags = parse_tags(tagseval,func_name,span_type,args,kwargs) 
            if span_type=="local":
                context = get_context()    
                with context.new_local_span(op=op) as span:
                    span.layer = layer
                    span.component = component
                    for key in tags:
                        span.tag(Tag(str(tags[key])).custom_key(str(key)))
                    return await func(*args, **kwargs)
            elif span_type=="exit":
                context = get_context()
                with context.new_exit_span(op=op, peer = peer) as span:
                    span.layer = layer
                    span.component = component
                    carrier = span.inject()
                    #kwargs[carrier_key] = carrier.val
                    return await func(*args, **kwargs)
            elif span_type=="entry":
                context = get_context()
                carrier = Carrier()
                if kwargs.get(carrier_key):
                    carrier.val = kwargs[carrier_key]
                with context.new_entry_span(op=op, carrier = carrier) as span:
                    span.layer = layer
                    span.component = component
                    return await func(*args, **kwargs)
            else:
                return await func(*args, **kwargs)
        try: 
            if inspect.iscoroutinefunction(func):
            #if is_coroutine(func):
                print("use 异步",func.__name__)
                return async_wrapper
            else:
                print("use 同步",func.__name__)
                return sync_wrapper
        except:
            print("异步使用with")
            print("????",type(func))
            return async_wrapper

    #import dbfortess.aiozmq as aiozmq
    #aiozmq.ZmqStream.write = wrap(aiozmq.ZmqStream.write,note="QQQ",span_type="local")
    
    #import dbfortess.aio as aio
    #aio.thread.AIOExecutor.call_soon = wrap(aio.thread.AIOExecutor.call_soon,note="ZZZ",span_type="local",
    #       tagseval="[('args',str(args)),('kwargs',str(kwargs))]")
    #aio.thread.AIOExecutor.call_soon_wait = wrap(aio.thread.AIOExecutor.call_soon_wait,note="YYY",span_type="local",  
    #       tagseval="[('args',str(args)),('kwargs',str(kwargs))]")
    #aio.thread.AIOExecutor.call_soon_ctx = wrap(aio.thread.AIOExecutor.call_soon_ctx,note="WWW",span_type="local", 
    #       tagseval="[('args',str(args)),('kwargs',str(kwargs))]")
    #aio.thread.AIOExecutor.run_in_executor = wrap(aio.thread.AIOExecutor.run_in_executor,note="VVV",span_type="local",
    #       tagseval="{'args':str(args),'kwargs':str(kwargs)}")
   
    #import  dbfortess.apm as apm
    #apm.hawkeye.HawkEye.on_key_event = wrap(apm.hawkeye.HawkEye.on_key_event,note="KKKK",span_type="local",
    #    tagseval="[('args',str(args)),('kwargs',str(kwargs))]")
    #apm.hawkeye.HawkEye.service_begin = wrap(apm.hawkeye.HawkEye.service_begin,note="BEGIN",span_type="local")
    #apm.hawkeye.HawkEye.service_end   = wrap(apm.hawkeye.HawkEye.service_end,note="END",span_type="local")


    #from dbfortess.service.base import ServiceDelegate    
    #from dbfortess.reactnet.client import VLANRequest
    
    #import dbfortess.reactnet.client as client
    #client.UMethod.__call__ = wrap(client.UMethod.__call__,note="ZZZZ")  
  
    import dbfortess.rpc.web as web
    #web.rpcrequest_get = wrap(web.rpcrequest_get,note="ONE",span_type="local") #无作用
    #web.rpcrequest_post = wrap(web.rpcrequest_post,note="two",span_type="local") #无作用
    #web.rpc_portal_index  = wrap(web.rpc_portal_index,note="three",span_type="local") #无作用
    #web.rpc_portal = wrap(web.rpc_portal,note = "THREE",span_type="local") #无作用
    
    #web.RPCGateWay.request_get = wrap(web.RPCGateWay.request_get,note="four",span_type="local",
    #    tagseval="{'args':str(args),'kwargs':str(kwargs)}")  #web端
    #web.RPCGateWay.request_post = wrap(web.RPCGateWay.request_post,note="five",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web端
    #web.WebSocketGateWay.__common_handler__ = wrap(web.WebSocketGateWay.__common_handler__,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #不起作用
    #web.WebService.__common_handler__ = wrap(web.WebService.__common_handler__,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web 1 
    #web.WebService.__request__ = wrap(web.WebService.__request__,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web 2 启用 

    #web.RPCClient.interceptor = wrap(web.RPCClient.interceptor,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #无作用 
    #web.StaticGateWay.index = wrap(web.StaticGateWay.index,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web 端 index 
    #web.StaticGateWay.static = wrap(web.StaticGateWay.static,note="000",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web 端 static

    #web.rpcrequest_vlans = wrap(web.rpcrequest_vlans,note="six")
    #web.rpcrequest_ws_get = wrap(web.rpcrequest_ws_get,note="seven")
    #web.rpcrequest_switch_vlans = wrap(web.rpcrequest_switch_vlans,note="eight")
    #web.rpcrequest_switches = wrap(web.rpcrequest_switches,note="nine")
    #web.rpcrequest_vlan_switches = wrap(web.rpcrequest_vlan_switches,note="ten")
    
    #import dbfortess.web.engine as engine
    #engine.request = wrap(engine.request,note="RRRR")
    #engine.WebService.__common_handler__ = wrap(engine.WebService.__common_handler__,note="WWW",span_type="local") #在quart后
    
    #import dbfortess.web.client as client
    #client.async_get = wrap(client.async_get,note="OOO",span_type="local")     
    #client.async_other = wrap(client.async_other,note="PPP",span_type="local")
    #client.get = wrap(client.get,note="QQQ",span_type="local")
    #client.other = wrap(client.other, note="RRR",span_type="local")
    
    #from ddtrace.span import Span
    #Span.__init__ = wrap(Span.__init__, note="SSS",span_type="local")
    
    import dbfortess.service.base as base
    #base.ServiceMeta.__call__ = wrap(base.ServiceMeta.__call__,note="dbfortess.service.base.ServiceMeta.__call_",span_type="local",
    #       tagseval="{'args':str(args),'kwargs':str(kwargs)}") # web端+rpc端 启动时注册的各项服务类
    #base.ServiceFactory.exit_hook = wrap(base.ServiceFactory.exit_hook,note="dbfortess.service.base.ServiceFactory.exit_hook",span_type="local",
    #       tagseval="{'args':str(args),'kwargs':str(kwargs)}") #无作用
    #base.ServiceFactory.api = wrp(base.ServiceFactory.api,note="base.ServiceFactory.api",span_type="local",
    #       tagseval="{'args':str(args),'kwargs':str(kwargs)}") #无作用
    #base.ServiceFactory.StaticServiceBase.index = wrap(base.ServiceFactory.StaticServiceBase.index,note="StaticServiceBase.index",span_type="local",
    #       tagseval="{'args':str(args),'kwargs':str(kwargs)}") # 可能在index服务起作用
    
    #import dbfortess.web.engine as engine
    #engine.WebEngine.parse_request_data = wrap(engine.WebEngine.parse_request_data,note="9999",span_type='local',
    #   tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web端  

    #import dbfortess.znet as znet
    #znet.ZClient.send_multipart = wrap(znet.ZClient.send_multipart,note="AAA",filtereval='args[2].decode()=="vlan_info"')
    #znet.ZServer.send_multipart = wrap(znet.ZServer.send_multipart,note="BBB",span_type="local")
    #znet.ZSocket.send_multipart = wrap(znet.ZSocket.send_multipart,note="CCC",span_type="local")
    #znet.ZIOSocket.send_multipart = wrap(znet.ZIOSocket.send_multipart,note="DDD",span_type="local",filtereval='args[2].decode()=="vlan_info"')
    
    import dbfortess.reactnet as reactnet
    #reactnet.client.ReactClient.__vlan_request__ = wrap(reactnet.client.ReactClient.__vlan_request__,note="XXX",span_type="local") #无作用
    #reactnet.client.ReactClient.__vlan_request_async__ = wrap(reactnet.client.ReactClient.__vlan_request_async__,note="YYY",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web端 rpc服务端结束
    #reactnet.client.ReactClient.on_rpc_vlan_response = wrap(reactnet.client.ReactClient.on_rpc_vlan_response,note="YYY",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #web端 产生独立的span ，暂时关闭
    
    #reactnet.client.ReactClient.on_vlan_response = wrap(reactnet.client.ReactClient.on_vlan_response,note="YYY",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #无作用

    #net.client.ReactClient.on_rpc_vlan_response = wrap(net.client.ReactClient.on_rpc_vlan_response,note="BBB",num=200,span_type="local")
    #net.client.ReactClient.on_vlan_response = wrap(net.client.ReactClient.on_vlan_response,note="AAA")
    #ServiceDelegate.__async_handler__ = wrap(ServiceDelegate.__async_handler__ )
    #ServiceDelegate.__handler__ = wrap(ServiceDelegate.__handler__) 
    #ServiceDelegate.__execute__ =wrap(ServiceDelegate.__execute__)


    #VLANRequest.__call__=wrap(VLANRequest.__call__,note="测试web",span_type="exit",
    #          opeval='kwargs.get("_request_scope",{})["method"]+kwargs.get("_request_scope",{})["path"]')
    
    #from dbfortess.rpc.worker import RPCBase
    #RPCBase.__handler__ = wrap(RPCBase.__handler__,note="测试rpc",span_type="entry",
    #     opeval='kwargs.get("_request_scope",{})["method"]+kwargs.get("_request_scope",{})["path"]')
    
    #from dbfortess.reactnet.vlan import VLANEngine
    #VLANEngine.__on_switch_received__ = wrap(VLANEngine.__on_switch_received__,note="测试 vlan",span_type="entry",
    #      num=500,filtereval="args[2][1].decode()=='vlan_info'",opeval = 'str(args[2][3].decode() +"/" +args[2][4].decode())')
    
    #from dbfortess.reactnet.vlan import VLANEngine
    #VLANEngine.on_rpc_vlan_request = wrap(VLANEngine.on_rpc_vlan_request,note="测试vlan_request",
    #      opeval="f'args[5]/args[6]'",span_type="exit")
    
    #import dbfortess.rpc.trace as trace
    #trace.Trace.get_now_trace = wrap(trace.Trace.get_now_trace,note="TRACE",span_type="local",
    #     tagseval="{'args':str(args),'kwargs':str(kwargs)}") #无作用   


    #from aiozmq import rpc 
    #rpc.rpc._ClientProtocol.call = wrap(rpc.rpc._ClientProtocol.call,note="测试 rpc.rpc._ClientProtocol.call",
                                        #opeval='f"[rpc_client]:{args[1]}"',
                                        #peereval = "args[0].transport.getsockopt(32)",imports=["zmq"])
    #rpc.rpc._ServerProtocol.msg_received = wrap(rpc.rpc._ServerProtocol.msg_received,
    #                                            opeval = 'f"[rpc_server]:{args[1][-3].decode()}"',
    #                                            note="测试 rpc.rpc._ServerProtocol.msg_received")
    #from dbfortess.patch import _pack_unknown_handler
    #_pack_unknown_handler = wrap(_pack_unknown_handler,note="error??????????") 
    print("test plugin finish","*"*20)
