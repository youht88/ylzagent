#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import requests
from google.protobuf import json_format

from ylzagent import config
from ylzagent.client import ServiceManagementClient, TraceSegmentReportService, LogDataReportService, MeterReportService
from ylzagent.loggings import logger, logger_debug_enabled
import time

class HttpServiceManagementClient(ServiceManagementClient):
    def __init__(self):
        super().__init__()
        self.instance_properties = self.get_instance_properties()

        proto = 'https://' if config.agent_force_tls else 'http://'
        self.url_instance_props = f"{proto}{config.agent_collector_backend_services.rstrip('/')}/v3/management/reportProperties"
        self.url_heart_beat = f"{proto}{config.agent_collector_backend_services.rstrip('/')}/v3/management/keepAlive"
        self.session = requests.Session()

    def send_instance_props(self):
        res = self.session.post(self.url_instance_props, json={
            'service': config.agent_name,
            'serviceInstance': config.agent_instance_name,
            'properties': self.instance_properties,
        })
        if logger_debug_enabled:
            logger.debug('heartbeat response: %s', res)

    def send_heart_beat(self):
        self.refresh_instance_props()

        if logger_debug_enabled:
            logger.debug(
                'service heart beats, [%s], [%s]',
                config.agent_name,
                config.agent_instance_name,
            )
        res = self.session.post(self.url_heart_beat, json={
            'service': config.agent_name,
            'serviceInstance': config.agent_instance_name,
        })
        if logger_debug_enabled:
            logger.debug('heartbeat response: %s', res)


class HttpTraceSegmentReportService(TraceSegmentReportService):
    def __init__(self):
        proto = 'https://' if config.agent_force_tls else 'http://'
        self.url_report = f"{proto}{config.agent_collector_backend_services.rstrip('/')}/v3/segment"
        self.session = requests.Session()

    def report(self, generator):
        for segment in generator:
            #print("组织segment")
            tag_requestIp = None
            tag_session = None
            tag_loginUser = None
            tag_return_value = None
                        
            try:
                #iindex = 0
                #for sitem in segment.spans:
                #    iindex = iindex + 1
                #    print(iindex,'跨度span=',str(sitem),)
                for tag in segment.spans[-1].iter_tags():
                    if tag.key == 'requestIp':
                        tag_requestIp = tag.val
                    if tag.key == 'session':
                        tag_session = tag.val
                    if tag.key == 'loginUser':
                        tag_loginUser = tag.val
                    if tag.key == 'returnValue':
                        tag_return_value = tag.val
                        tag.val = ""
            except Exception as e:
                print("!!!!!!!!!!!!!!!",e)
            
            #tag_loginUser=None
            #tag_return_value=None
            #tag_session = None
            #print('segment???',tag_session,tag_loginUser,tag_return_value)
            #print("segemnt??",self.url_report)            
            payload = {
                'traceId': str(segment.related_traces[0]),
                'traceSegmentId': str(segment.segment_id),
                'service': config.agent_name,
                'serviceInstance': config.agent_instance_name,
                'spans': [{
                    'spanId': span.sid,
                    'parentSpanId': span.pid,
                    'startTime': span.start_time,
                    'endTime': span.end_time,
                    'operationName': span.op,
                    'peer': span.peer,
                    'spanType': span.kind.name,
                    'spanLayer': span.layer.name,
                    'componentId': span.component.value,
                    'isError': span.error_occurred,
                    'logs': [{
                        'time': int(log.timestamp * 1000),
                        'data': [{
                            'key': item.key,
                            'value': item.val,
                        } for item in log.items],
                    } for log in span.logs],
                    'tags': [{
                        'key': tag.key,
                        'value': tag.val,
                    } for tag in span.iter_tags()],
                    'refs': [{
                        'refType': 0,
                        'traceId': ref.trace_id,
                        'parentTraceSegmentId': ref.segment_id,
                        'parentSpanId': ref.span_id,
                        'parentService': ref.service,
                        'parentServiceInstance': ref.service_instance,
                        'parentEndpoint': ref.endpoint,
                        'networkAddressUsedAtPeer': ref.client_address,
                    } for ref in span.refs if ref.trace_id]
                } for span in segment.spans],
                "requestIp": '' if tag_requestIp == None else tag_requestIp,   
                "session": '' if tag_session == None else tag_session,
                "loginUser": '' if tag_loginUser == None else tag_loginUser,
                "returnValue": '' if tag_return_value == None else tag_return_value # segment.spans[0]
            }
            #print("json???",self.url_report,payload)
            res = self.session.post(self.url_report,json = payload)
            if logger_debug_enabled:
                logger.debug('report traces response: %s', res)


class HttpLogDataReportService(LogDataReportService):
    def __init__(self):
        proto = 'https://' if config.agent_force_tls else 'http://'
        self.url_report = f"{proto}{config.agent_collector_backend_services.rstrip('/')}/v3/logs"
        self.session = requests.Session()

    def report(self, generator):
        log_batch = [json.loads(json_format.MessageToJson(log_data)) for log_data in generator]
        if log_batch:  # prevent empty batches
            res = self.session.post(self.url_report, json=log_batch)
            if logger_debug_enabled:
                logger.debug('report batch log response: %s', res)

class HttpMeterReportService(MeterReportService):
    def __init__(self):
        proto = 'https://' if config.agent_force_tls else 'http://'
        self.url_report = f"{proto}{config.agent_collector_backend_services.rstrip('/')}/v3/meterData"
        self.session = requests.Session()
        #self.meterData = []
        #self.start_time = time.time()
        
    def report(self, generator):
        meter_batch = [json.loads(json_format.MessageToJson(meter_data)) for meter_data in generator]
        if meter_batch:  # prevent empty batches
            payload = {"meterData": meter_batch}
            res = self.session.post(self.url_report, json=payload)
            #logger.info(f"meter report==>{res},{len(meter_batch)}")
            if logger_debug_enabled:
                logger.debug('report batch log response: %s', res)

        # for meter in generator:
        #     #print("HERE!!!==>",meter)
        #     try:
        #         self.meterData.append({
        #           "singleValue":{
        #             "name": meter.singleValue.name,
        #             "value": meter.singleValue.value,
        #             "labels": [{"name":label.name,"value":label.value} for label in meter.singleValue.labels] 
        #                   if hasattr(meter.singleValue,"labels") else None
        #           },
        #           "service": meter.service,
        #           "serviceInstance": meter.serviceInstance,
        #           "timestamp": meter.timestamp
        #           })
        #     except Exception as e:
        #         print("http meter report error:",e)
        #     #print("report data -->",payload)
        #     diff_time = time.time() - self.start_time
        #     delayCommitSecond = int(config.agent_commit_efficiency.get('delayCommitSecond','1'))
        #     delayCommitNum = int(config.agent_commit_efficiency.get('delayCommitNum','50'))
        #     if ( diff_time<=delayCommitSecond and len(self.meterData)>=delayCommitNum ) or (diff_time>delayCommitSecond):
        #         payload = {"meterData":self.meterData}
        #         res = self.session.post(self.url_report, json=payload)
        #         #print("http meter report==>",diff_time,len(self.meterData),delayCommitSecond,delayCommitNum,payload,res)
        #         self.meterData = []
        #         self.start_time = time.time()
        #     if logger_debug_enabled:
        #         logger.debug('report meter response: %s', res)

